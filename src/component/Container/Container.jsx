import React from "react";
import styles from "./Container.module.scss";

const Container = (props) => {
  var cls = ["container-fluid", styles.Container];
  return <div className={cls.join(" ")}>{props.children}</div>;
};

export default Container;
