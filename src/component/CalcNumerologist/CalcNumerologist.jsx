import React, { Component } from "react";
import FormCalc from "../Form/FormCalc";
import ResultCalc from "../Result/ResultCalc";
import styles from "./CalcNumerologist.module.scss";
class CalcNumerologist extends Component {
  state = {};

  /**
   * Создание компонента
   */
  constructor(props) {
    super(props);

    this.state = {
      IsValid: true,
      data: {
        IsSubmit: false,
        ValueDate: "--.--.----",
        NumberAdditionalNumber: "--.--  --.--",
        NumberFate: "---",
        character: "---",
        health: "---",
        luck: "---",
        energy: "---",
        logics: "---",
        debt: "---",
        interest: "---",
        work: "---",
        memory: "---",
        temperament: "---",
        everyday: "---",
        target: "---",
        family: "---",
        habits: "---",
      },
    };
  }

  setIsValid = (value) => {
    this.setState({
      isValid: value,
    });
  };

  handleSubmit = (e) => {
    e.preventDefault();
    var form = e.target;
    var formData = new FormData(form);

    var value = formData.get("date");

    var arr;
    var dateOfBirth = value;

    var strDate = dateOfBirth + "";
    var aDate = strDate.split(".");
    if (aDate.length != 3) {
      this.setState({
        IsValid: false,
      });
      return;
    }

    var nDay = parseInt(aDate[0]);
    var nMonth = parseInt(aDate[1]);
    var nYear = parseInt(aDate[2]);

    // Доп. числа
    // Первое число
    var numberOne = 0;
    if (nDay < 10) {
      numberOne += nDay;
    } else {
      arr = (nDay + "").split("");
      for (let i = 0; i < arr.length; i++) {
        numberOne += parseInt(arr[i]);
      }
    }
    if (nMonth < 10) {
      numberOne += nMonth;
    } else {
      arr = (nMonth + "").split("");
      for (let i = 0; i < arr.length; i++) {
        numberOne += parseInt(arr[i]);
      }
    }
    arr = (nYear + "").split("");
    for (let i = 0; i < arr.length; i++) {
      numberOne += parseInt(arr[i]);
    }
    numberOne = parseInt(numberOne);
    // Второе число
    var numberTwo = 0;
    if (numberOne < 10) {
      numberTwo = numberOne;
    } else {
      arr = (numberOne + "").split("");
      for (let i = 0; i < arr.length; i++) {
        numberTwo += parseInt(arr[i]);
      }
    }
    numberTwo = parseInt(numberTwo);
    // Третье число
    var numberThree = 0;
    if (nDay < 10) {
      numberThree = numberOne - 2 * nDay;
    } else {
      arr = (nDay + "").split("");
      numberThree = numberOne - 2 * arr[0];
    }
    numberThree = parseInt(numberThree);
    // Четвертое число
    var numberFour = 0;
    if (numberThree < 10) {
      numberFour = numberThree;
    } else {
      arr = (numberThree + "").split("");
      for (let i = 0; i < arr.length; i++) {
        numberFour += parseInt(arr[i]);
      }
    }
    numberFour = parseInt(numberFour);

    // Число судьбы
    var numberFate = 0;
    arr = (numberOne + "").split("");
    for (let i = 0; i < arr.length; i++) {
      numberFate += parseInt(arr[i]);
    }

    for (let i = 0; i < 99; i++) {
      if (numberFate < 10 || numberFate == 11) {
        break;
      }
      arr = (numberFate + "").split("");
      numberFate = 0;
      for (let j = 0; j < arr.length; j++) {
        numberFate += parseInt(arr[j]);
      }
    }

    // Квадрат пифагора
    var strNumberAll = "" + nDay + nMonth + nYear + numberOne + numberTwo + numberThree + numberFour;
    arr = strNumberAll.split("");

    // Характер
    var character = "";
    for (let i = 0; i < arr.length; i++) {
      if (parseInt(arr[i]) == 1) {
        character += arr[i];
      }
    }
    // Энергия
    var energy = "";
    for (let i = 0; i < arr.length; i++) {
      if (parseInt(arr[i]) == 2) {
        energy += arr[i];
      }
    }
    // Интерес
    var interest = "";
    for (let i = 0; i < arr.length; i++) {
      if (parseInt(arr[i]) == 3) {
        interest += arr[i];
      }
    }
    // Здоровье
    var health = "";
    for (let i = 0; i < arr.length; i++) {
      if (parseInt(arr[i]) == 4) {
        health += arr[i];
      }
    }
    // Логика
    var logics = "";
    for (let i = 0; i < arr.length; i++) {
      if (parseInt(arr[i]) == 5) {
        logics += arr[i];
      }
    }
    // Труд
    var work = "";
    for (let i = 0; i < arr.length; i++) {
      if (parseInt(arr[i]) == 6) {
        work += arr[i];
      }
    }
    // Удача
    var luck = "";
    for (let i = 0; i < arr.length; i++) {
      if (parseInt(arr[i]) == 7) {
        luck += arr[i];
      }
    }
    // Долг
    var debt = "";
    for (let i = 0; i < arr.length; i++) {
      if (parseInt(arr[i]) == 8) {
        debt += arr[i];
      }
    }
    //  Память
    var memory = "";
    for (let i = 0; i < arr.length; i++) {
      if (parseInt(arr[i]) == 9) {
        memory += arr[i];
      }
    }

    // Темперамент
    var temperament = interest.length + logics.length + luck.length;

    // Быт
    var everyday = health.length + logics.length + work.length;

    // Цель
    var target = character.length + health.length + luck.length;

    // Семья
    var family = energy.length + logics.length + debt.length;

    // Привычки
    var habits = interest.length + work.length + memory.length;

    var day = nDay + "";
    if (nDay < 10) {
      day = "0" + day;
    }
    var month = nMonth + "";
    if (nMonth < 10) {
      month = "0" + month;
    }

    var ValueDate = day + "." + month + "." + nYear;
    var NumberAdditionalNumber = numberOne + "." + numberTwo + " " + numberThree + "." + numberFour;
    var NumberFate = numberFate;
    if (character == "") character = "---";
    if (health == "") health = "---";
    if (luck == "") luck = "---";
    if (energy == "") energy = "---";
    if (logics == "") logics = "---";
    if (debt == "") debt = "---";
    if (interest == "") interest = "---";
    if (work == "") work = "---";
    if (memory == "") memory = "---";
    if (temperament == "") temperament = "---";
    if (everyday == "") everyday = "---";
    if (target == "") target = "---";
    if (family == "") family = "---";
    if (habits == "") habits = "---";

    var isSubmit = !this.state.data.IsSubmit;
    this.setState({
      IsValid: true,
      data: {
        IsSubmit: isSubmit,
        ValueDate: ValueDate,
        NumberAdditionalNumber: NumberAdditionalNumber,
        NumberFate: NumberFate,
        character: character,
        health: health,
        luck: luck,
        energy: energy,
        logics: logics,
        debt: debt,
        interest: interest,
        work: work,
        memory: memory,
        temperament: temperament,
        everyday: everyday,
        target: target,
        family: family,
        habits: habits,
      },
    });
  };

  /**
   * Отрисовка компонента
   * @returns HTML Render
   */
  render() {
    var cls = ["row", styles.Row];

    return (
      <React.Fragment>
        <div className={cls.join(" ")}>
          <div className="col-12 col-md-5">
            <FormCalc isValid={this.state.IsValid} onSubmit={this.handleSubmit} />
          </div>
          <div className="col-12 col-md-7">
            <ResultCalc data={this.state.data} />
          </div>
        </div>
      </React.Fragment>
    );
  }

  /**
   * Выполняется после отрисовки компонента
   */
  componentDidMount() {
    document.title = "Ваш нумерлог | Онлайн калькулятор";
  }
}
export default CalcNumerologist;
