import React from "react";
import styles from "./FormCalc.module.scss";
import InputMask from "react-input-mask";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
function FormCalc(props) {
  const [show, setShow] = React.useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  var cls = ["form-calc"];
  if (!props.isValid) {
    cls.push(styles.InValid);
  }
  return (
    <div className={styles.Container}>
      <form onSubmit={props.onSubmit} id="FormCalc" className={cls.join(" ")}>
        <div className="form-group d-flex flex-column text-center">
          <div className="h2 mb-3">
            <b>Введите дату рождения:</b>
          </div>
          <div className={styles.FormGroup}>
            <InputMask type={"text"} mask="99.99.9999" name="date" className={styles.Input} placeholder="ДД.ММ.ГГГГ" />
          </div>
          <button type="submit" className={styles.Submit}>
            Рассчитать
          </button>
        </div>
      </form>
      {!props.isValid ? (
        <div className="card text-white bg-danger mt-3">
          <div className="card-header">Внимание !</div>
          <div className="card-body">
            <h5 className="card-title">Неверный формат даты</h5>
          </div>
        </div>
      ) : null}
    </div>
  );
}

export default FormCalc;
